apk update
apk add sudo
adduser -D stick
echo -e "stick\nstick" | passwd stick
echo '%wheel ALL=(ALL) ALL' > /etc/sudoers.d/wheel
adduser stick wheel
apkver=$(grep -om 1 'v\d*\.\d*' /etc/apk/repositories)
sed -i "/$apkver/s/^#//g" /etc/apk/repositories
apk update
apk add xfce4 xfce4-terminal lightdm-gtk-greeter networkmanager network-manager-applet iwd chromium
rc-update add dbus
rc-update add lightdm
rc-update add iwd
rc-update add networkmanager
echo -e "\n[device]\nwifi.backend=iwd" >> /etc/NetworkManager/NetworkManager.conf
setup-xorg-base
reboot